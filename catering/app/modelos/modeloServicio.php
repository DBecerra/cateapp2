<?php

include_once '../librerias/db-connect.php';

class ServicioModelo{
	private $db;
	private $db_table = "servicio";
	public function __construct(){
		$this->db = new DbConnect();

	}
	

	public function mostrarRegistros(){
		$query = "SELECT * from ".$this->db_table;
		$result = mysqli_query($this->db->getDb(),$query);
		if(mysqli_num_rows($result) > 0){
			$json = array();
			$i=0;
 			while($row = mysqli_fetch_assoc($result)){
 			  				 				
				$json['estudiantes'][]=$row;
			 			}
			
 			mysqli_close($this->db->getDb());
			return $json;
 		}else{
		
		mysqli_close($this->db->getDb());
		return false;}
	}

	public function agregarServicio($datos){	
		$json = array();
		$query = "INSERT INTO ".$this->db_table."(SerTipSer, SerUsuId, SerDes, SerPre, SerFot, SerEstReg) VALUES ('$datos[0]', '$datos[1]', '$datos[2]', '$datos[3]', '$datos[4]', 16)";

		$inserted = mysqli_query($this->db->getDb(), $query);
		if($inserted == 1){
			$json['success'] = 1;
			$json['message'] = "Servicio registrado con exito";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error al registrar servicio";
		}
		mysqli_close($this->db->getDb());
			
		
		return $json;
	}

}
?>