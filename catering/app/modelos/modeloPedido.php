<?php

include_once '../librerias/db-connect.php';

class PedidoModelo{
	private $db;
	private $db_table = "movimientocabecera";
	private $db_table1 = "movimientodetalle";
	public function __construct(){
		$this->db = new DbConnect();

	}
	

	public function mostrarRegistros($id){
		$query = "SELECT * FROM movimientocabecera INNER JOIN usuario on usuario.UsuId = movimientocabecera.MovCabUsuIdCli where MovCabUsuIdPro = $id";
		$result = mysqli_query($this->db->getDb(),$query);
		if(mysqli_num_rows($result) > 0){
			$json = array();
			$i=0;
 			while($row = mysqli_fetch_assoc($result)){
 			  		$det=$row["UsuNom"]." ".$row["UsuApePat"]." ".$row["UsuApeMat"];				
				 $jsonArrayObject = (array('MovCabUsuIdCli' =>$det , 'MovCabFec' => $row["MovCabFec"], 'MovCabUbi' => $row["MovCabUbi"]));
                $arr[$i] = $jsonArrayObject;
                $i++;
			 			}
			 			$json_array = json_encode($arr);
            
			
 			mysqli_close($this->db->getDb());
			return $json_array;
 		}else{
		
		mysqli_close($this->db->getDb());
		return false;}
	}


	

	public function agregarPedido($datos){	
		$json = array();
		$query = "INSERT INTO ".$this->db_table."(MovCabUsuIdPro,MovCabUsuIdCli,MovCabFec, MovCabUbi, MovCabTipPag, MovCabEstReg) VALUES (1,1,'$datos[1]','$datos[2]','$datos[3]',16)";
		$inserted = mysqli_query($this->db->getDb(), $query);

		
		$query1 = "SELECT movimientocabecera.MovCabId as ultimo
FROM movimientocabecera
ORDER by movimientocabecera.MovCabId DESC LIMIT 0,1";
		$idCabe=mysqli_query($this->db->getDb(),$query1);
		


		while($row = mysqli_fetch_assoc($idCabe)){
 			  				 				
				$idCabe1=$row['ultimo'];
			 			}
        $query2 = "INSERT INTO ".$this->db_table1."(ModDetMovCabId,MovDetSerId, ModDetCan, ModDetEstReg) VALUES ('$idCabe1','$datos[0]','$datos[4]',16)";
	
		$inserted2 = mysqli_query($this->db->getDb(), $query2);


		if($inserted == 1 ){
			$json['success'] = 1;
			$json['message'] = "Pedidoregistrado con exito";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error al registrar pedido";
		}
		mysqli_close($this->db->getDb());
			
		
		return $json;
	}

}
?>