<?php

include_once '../librerias/db-connect.php';

class UsuarioModelo{
	private $db;
	private $db_table = "usuario";
	public function __construct(){
		$this->db = new DbConnect();

	}
	

	public function mostrarRegistros(){
		$query = "SELECT * from ".$this->db_table;
		$result = mysqli_query($this->db->getDb(),$query);
		if(mysqli_num_rows($result) > 0){
			$json = array();
			$i=0;
 			while($row = mysqli_fetch_assoc($result)){
 			  				 				
				$json['estudiantes'][]=$row;
			 			}
			
 			mysqli_close($this->db->getDb());
			return $json;
 		}else{
		
		mysqli_close($this->db->getDb());
		return false;}
	}


	


	public function agregarUsuario($datos){	
		$json = array();
		$query = "INSERT INTO ".$this->db_table."(UsuTipUsu, UsuApePat, UsuApeMat, UsuNom, UsuTel, UsuCorEle, UsuUsu, UsuPass, UsuFecNac, UsuEstReg) VALUES ('$datos[0]', '$datos[1]', '$datos[2]', '$datos[3]', '$datos[4]', '$datos[5]', '$datos[6]', '$datos[7]', '$datos[8]', 16)";
		$inserted = mysqli_query($this->db->getDb(), $query);
		if($inserted == 1){
			$json['success'] = 1;
			$json['message'] = "Cliente registrado con exito";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error al registrar usuario";
		}
		mysqli_close($this->db->getDb());
			
		
		return $json;
	}


	public function agregarProveedor($datos){	
		$json = array();
		$query = "INSERT INTO ".$this->db_table."(UsuTipUsu, UsuRazSoc, UsuRUC, UsuApePat, UsuApeMat, UsuNom, UsuDir, UsuDis, UsuPro, UsuDep, UsuPagWeb, UsuTel, UsuCorEle, UsuUsu, UsuPass, UsuEstReg) VALUES ('$datos[0]', '$datos[1]', '$datos[2]', '$datos[3]', '$datos[4]', '$datos[5]', '$datos[6]', '$datos[7]', '$datos[8]', '$datos[9]', '$datos[10]', '$datos[11]', '$datos[12]', '$datos[13]', '$datos[14]', 16)";

		$inserted = mysqli_query($this->db->getDb(), $query);
		if($inserted == 1){
			$json['success'] = 1;
			$json['message'] = "Proveedor registrado con exito";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error al registrar usuario";
		}
		mysqli_close($this->db->getDb());
			
		
		return $json;
	}

	public function login($datos){
$json = array();
$query = "SELECT * FROM usuario WHERE usuario.UsuUsu = '$datos[0]' AND usuario.UsuPass='$datos[1]' AND usuario.UsuEstReg =16";

$signin = mysqli_query($this->db->getDb(), $query);

$usuario="usuario";
if(mysqli_num_rows($signin) > 0){
$usuario="ss";
  if($row = mysqli_fetch_assoc($signin)){
  $json['success'] = 1;
  $json['idCliente'] = $row['UsuId'];
$json['message'] = "Bienvenido ".$row['UsuNom'];
//$usuario=$row->UsuNom;
}
  }

if(mysqli_num_rows($signin) > 0){
//$json['success'] = 1;
//$json['message'] = "Bienvenido ".$usuario;
}else{
$json['success'] = 0;
$json['idCliente'] = 0;
$json['message'] = "Usuario o Contraseña incorrecto";
}
mysqli_close($this->db->getDb());


return $json;
}

public function obtenerServiciosdeCliente($datos){
		$query = "SELECT CONCAT(usupro.UsuNom,' ',usupro.UsuApePat) AS proveedor, gentp.GenSubCat AS pago, gentd.GenSubCat AS documento, serv.SerDes AS servicio,serv.SerPre AS precio
	FROM movimientocabecera AS movcab
    LEFT JOIN usuario As usupro ON 	usupro.UsuId=movcab.MovCabUsuIdPro
    LEFT JOIN general AS gentp ON gentp.GenId=movcab.MovCabTipPag
    LEFT JOIN general AS gentd ON gentd.GenId=movcab.MovCabTipDoc
    LEFT JOIN movimientodetalle AS md ON md.ModDetMovCabId=movcab.MovCabId
    LEFT JOIN servicio AS serv ON serv.SerId=md.MovDetSerId
    WHERE movcab.MovCabUsuIdCli=".$datos[0];
		$result = mysqli_query($this->db->getDb(),$query);
		if(mysqli_num_rows($result) > 0){
			$json = array();
			$i=0;
 			while($row = mysqli_fetch_assoc($result)){
 			  				 				
				$json['pedidos'][]=$row;
			 			}
			
 			mysqli_close($this->db->getDb());
			return $json;
 		}else{
		
		mysqli_close($this->db->getDb());
		return false;}
	}


}
?>