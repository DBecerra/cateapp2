<?php
	//Para cargar todos lo archivos dentro de librerias/*
	spl_autoload_register(function($nombreClase){
		require_once '../modelos/'.$nombreClase.'.php';
	});

 	require_once '../modelos/modeloServicio.php';
 	$metodo=0;
 	//la variable metodo identificara la accion del controlador ya sea registrar, editar, mostrar etc

 	# Recibimos los datos leídos de php://input
	$datosRecibidos = file_get_contents("php://input");
	# No los hemos decodificado, así que lo hacemos de una vez:

	$getDatos = json_decode($datosRecibidos);
	# Ahora podemos acceder a los datos
	$metodo = $getDatos->metodo;

 	$servicio = new ServicioModelo();

 	// mostrar datos
	if($metodo==1){
 		$json_mostrar = $servicio->mostrarRegistros();
 		echo json_encode($json_mostrar,JSON_FORCE_OBJECT);
 	}

 	// Registrar pedido
 	if($metodo==6){
 		$SerTipSer = $getDatos->SerTipSer;
 		$SerUsuId= $getDatos->SerUsuId;
 		$SerDes = $getDatos->SerDes;
 		$SerPre = $getDatos->SerPre;
 		$SerFot = $getDatos->SerFot;

 		$datos=[$SerTipSer, $SerUsuId, $SerDes, $SerPre, $SerFot];

 		#echo $SerTipSer." ".$SerUsuId." ".$SerDes." ".$SerPre." ".$SerFot;
 		$json_registrar = $servicio->agregarServicio($datos);
 		echo json_encode($json_registrar);
 		/*
 		$string = JSON.stringify($json_registration);
 		$string = strrev($string);
 		$string = substr($string, 20);
 		$string = strrev($string)."}";
 		echo json_decode($string);*/
	}

 	// Login
 	/*if(!empty($username) && !empty($password) && empty($email)){
 		$hashed_password = md5($password);
 		$json_array = $servicio->loginUsers($username, $hashed_password);
 		echo json_encode($json_array);
 	}*/
 ?>
